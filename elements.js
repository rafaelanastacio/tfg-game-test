//////////////// RECTANGLE /////////////////


var Rectangle = function(width,height,vx,vy) {
    this.width = width;
    this.height = height;

    this.rect_x = 0;
    this.rect_y = 0;

    this.velocity = 20;

    // Handle keyboard controls
    this.keysDown = {};

    this.vx = (vx === undefined) ? 0 : vx;
    this.vx = (vy === undefined) ? 0 : vy;

    this.parent;

    // not user controlled by default
    this.isUserControlled = false;




};

Rectangle.prototype.draw = function(context){
	context.fillRect(this.rect_x, this.rect_y, this.width, this.height)
};

Rectangle.prototype.update = function(){

    // in case of being controlled by user
    if (37 in this.keysDown) { // Player holding left
        this.vx = - this.velocity;
    }
    if (39 in this.keysDown) { // Player holding right
        this.vx = this.velocity;
    }

    // finally change position
    this.rect_x += this.vx;
    this.rect_y += this.vy;
  
};

Rectangle.prototype.setUserControl = function (document) {

    this.isUserControlled = true;
    var that = this;

    document.addEventListener("keydown", function (e) {
        that.keysDown[e.keyCode] = true;
    }, false);

    document.addEventListener("keyup", function (e) {
        delete that.keysDown[e.keyCode];
        if(that.isUserControlled){
            that.vx = 0;
        }
    }, false);

}

Rectangle.prototype.attach = function(parent){
  this.parent = parent;
};

Rectangle.prototype.setVelocity = function(vx,vy) {
  this.vx = vx;
  this.vy = vy;
};
Rectangle.prototype.setPosition = function(rect_x,rect_y){
  this.rect_x = rect_x;
  this.rect_y = rect_y;
};

Rectangle.prototype.remove = function(index){
  this.parent.remove(index)
};

Rectangle.prototype.colision = function(){
    this.setVelocity(0,0);
    this.parent.notify("colision-start");
};

Rectangle.prototype.onTouchingBorder = function (border) {
    if(this.isUserControlled){
        if(border == "left"){
            // does nothing yet
        }
        if(border == "right"){
            // does nothing yet
        }

    }

};








/////////////////////////  
                    

                    //CANVAS//
                                /////////////////////











var Canvas = function(width,height, context){
  this.width=width;
  this.height=height;
  this.context = context;
  this.elements = [];
  this.parent;
};

Canvas.prototype.draw = function(){
  this.context.clearRect(0, 0, this.width, this.height);

  for (var i = this.elements.length - 1; i >= 0; i--) {
    this.elements[i].draw(this.context);
  }

};

Canvas.prototype.update = function(){
  
    

    for (var i = 0; i < this.elements.length ; i++) {
    this.elements[i].update();
    } 
  };
  

Canvas.prototype.add = function(element){
  element.attach(this);
  this.elements.push(element);
};

Canvas.prototype.attach = function(parent){
  this.parent = parent;

};



Canvas.prototype.check = function(element){
  if(element === undefined){
    //treating all elements
    
    // removing those out of canvas
    for (var i = this.elements.length - 1 ; i >= 0 ; i--) {
        if (this.getTouchingBorder(this.elements[i])){
            this.onTouchingBorder(this.getTouchingBorder(this.elements[i]),this.elements[i]);
        }

        if (this.isOutOfCanvas(this.elements[i])){
            this.elements[i].remove(i)
        }

        
    }

    // detecting colision

     for (var i = this.elements.length - 1 ; i >= 0 ; i--) {
       for (var j = i - 1 ; j >= 0 ; j--) {
      
        if (this.isThereColision(this.elements[i], this.elements[j])){
            this.elements[i].colision();
            this.elements[j].colision();
        }
      }
    }



  }else{

    // treating specific element
    if (isOutOfCanvas(element)){
      this.elements.pop(element)
    }
  }

};

Canvas.prototype.remove = function(index){
  this.elements.splice(index,1);
  this.parent.notify("obstacle-removed");

};

Canvas.prototype.notify = function(message){
  this.parent.notify(message);

};

Canvas.prototype.isOutOfCanvas = function(rectangle){

 statement = rectangle.rect_y > canvas.height || rectangle.rect_x > canvas.width;
 return statement
};

Canvas.prototype.isThereColision = function(r1, r2){

    // statement for never existing colision
    statement =    r1.rect_x > r2.rect_x + r2.width
                || r1.rect_x + r1.width < r2.rect_x
                || r1.rect_y + r1.height < r2.rect_y
                || r1.rect_y > r2.rect_y + r2.height

    return !statement;
};

Canvas.prototype.getTouchingBorder = function (element) {
    if(element.rect_x <= 0){
        return "left";
    }
    if(element.rect_x + element.width >= this.width){
        return "right";
    }
    return null;
};
Canvas.prototype.onTouchingBorder = function (border, element) {
    if(element.isUserControlled){
        if(border == "left"){
            element.setPosition(1,element.rect_y);
            element.setVelocity(0,element.vy)

        }
        if(border == "right"){
            element.setPosition(this.width - element.width  -1, element.rect_y);
            element.setVelocity(0,element.vy)
        }

    }

    element.onTouchingBorder(border);

};






