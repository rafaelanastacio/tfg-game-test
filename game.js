var Game = {};

Game.fps = 30;

var width;
var height;
var canvas;

var velocity;

Game.initialize = function() {
  this.entities = [];

  velocity = 10;
  context = document.getElementById("canvas").getContext("2d");
 
  width = document.getElementById("canvas").attributes.width.value
  height = document.getElementById("canvas").attributes.height.value
  
  canvas = new Canvas(width,height, context);

  canvas.attach(this);

  // =====
  // Example
  var rect1 = new Rectangle(100,100);
  rect1.setVelocity(0,velocity);
  canvas.add(rect1);

  var userCar = new Rectangle(100,100)
  userCar.setPosition(canvas.width/2- userCar.width/2 ,canvas.height - userCar.height/2 )
  userCar.setVelocity(0,0);
  userCar.setUserControl(document);
  canvas.add(userCar);
  // =====
};


Game.draw = function() {



  // Your code goes here
  canvas.draw();

  // =====
  // Example
  //=====
};


Game.update = function() {
  // Your code goes here

  // =====
  // Example
    canvas.check();
    canvas.update();

  

  // =====



};

Game.notify = function(message){

  switch(message) {
      case "obstacle-removed":
          var rect1 = new Rectangle(100,100);
          rect1.setPosition(Math.random()*canvas.width,0)
          rect1.setVelocity(0,velocity++);
          canvas.add(rect1);
      case "colision-start":
          return;

  }
    

  
}




